#!/bin/bash

source /opt/ros/noetic/setup.bash

rosdep update
mkdir -p ~/ws_moveit/src
cd ~/ws_moveit/src
wstool init .
wstool merge -t . https://raw.githubusercontent.com/ros-planning/moveit/master/moveit.rosinstall
wstool remove  moveit_tutorials  # this is cloned in the next section
wstool update -t .
echo 'source ~/ws_moveit/devel/setup.bash' >> ~/.bashrc
source ~/ws_moveit/devel/setup.bash

cd ~/ws_moveit/src
git clone https://github.com/ros-planning/moveit_tutorials.git -b master
git clone https://github.com/ros-planning/panda_moveit_config.git -b melodic-devel
cd ~/ws_moveit/src
rosdep install -y --from-paths . --ignore-src --rosdistro noetic
cd ~/ws_moveit
catkin config --extend /opt/ros/${ROS_DISTRO} --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin build

cd ~/ws_moveit/src
git clone -b melodic-devel https://github.com/ros-industrial/universal_robot
rosdep install -y --from-paths . --ignore-src --rosdistro noetic
cd ~/ws_moveit/
catkin build
