# !/ usr / bin / bash
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	        -- '[0.0 , 5.0 , 0.0]' '[0.0 , 0.0 , 4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	        -- '[1.0 , 5.0 , 0.0]' '[0.0 , 0.0 , -4.5]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	        -- '[0.0 , 3.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	        -- '[0.0 , 7.0 , 0.0]' '[0.0 , 0.0 , 6.0]'

